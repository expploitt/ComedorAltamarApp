package com.uvigo.watermelons.comedor;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static com.uvigo.watermelons.comedor.MainActivity.globalParameters;

public class ComidasActivity extends AppCompatActivity {

    Spinner lCo, lCe, mCo, mCe, xCo, xCe, jCo, jCe, vCo, vCe;
    TextView lunes, martes, miercoles, jueves, viernes;
    private GlobalParameters globalParameters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comidas);

        lCo = (Spinner) findViewById(R.id.spinner1);
        lCe = (Spinner) findViewById(R.id.spinner2);
        mCo = (Spinner) findViewById(R.id.spinner3);
        mCe = (Spinner) findViewById(R.id.spinner4);
        xCo = (Spinner) findViewById(R.id.spinner5);
        xCe = (Spinner) findViewById(R.id.spinner6);
        jCo = (Spinner) findViewById(R.id.spinner7);
        jCe = (Spinner) findViewById(R.id.spinner8);
        vCo = (Spinner) findViewById(R.id.spinner9);
        vCe = (Spinner) findViewById(R.id.spinner10);

        ArrayList<Spinner> spinners = new ArrayList<>();

        final GlobalParameters globalParameters = (GlobalParameters) getApplicationContext();

        spinners.add(lCo);
        spinners.add(lCe);
        spinners.add(mCo);
        spinners.add(mCe);
        spinners.add(xCo);
        spinners.add(xCe);
        spinners.add(jCo);
        spinners.add(jCe);
        spinners.add(vCo);
        spinners.add(vCe);

        lunes = (TextView) findViewById(R.id.lunes);
        martes = (TextView) findViewById(R.id.martes);
        miercoles = (TextView) findViewById(R.id.miercoles);
        jueves = (TextView) findViewById(R.id.jueves);
        viernes = (TextView) findViewById(R.id.viernes);

        for (Spinner spinner : spinners) {
            ArrayAdapter<CharSequence> adaptador = ArrayAdapter.createFromResource(this, R.array.horarios_array, android.R.layout.simple_spinner_item);
            adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // Apply the adapter to the spinner
            spinner.setAdapter(adaptador);
        }

        /*AHORA COMPROBAMOS LA FECHA A LA QUE ESTAMOS PARA VER QUE BOTONES DEBEMOS HABILITAR*/

        lCo.setSelection(queNumero(globalParameters.getLuCo()));
        lCe.setSelection(queNumero(globalParameters.getLuCe()));
        mCo.setSelection(queNumero(globalParameters.getMaCo()));
        mCe.setSelection(queNumero(globalParameters.getMaCe()));
        xCo.setSelection(queNumero(globalParameters.getMiCo()));
        xCe.setSelection(queNumero(globalParameters.getMiCe()));
        jCo.setSelection(queNumero(globalParameters.getJuCo()));
        jCe.setSelection(queNumero(globalParameters.getJuCe()));
        vCo.setSelection(queNumero(globalParameters.getViCo()));
        vCe.setSelection(queNumero(globalParameters.getViCe()));


        Calendar calendar = Calendar.getInstance();

        switch (calendar.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.MONDAY:

                lCo.setEnabled(false);

                mCo.setEnabled(false);

                lCe.setEnabled(false);

                mCe.setEnabled(false);
                lunes.setTextColor(ContextCompat.getColor(this, R.color.diaCaducado));
                martes.setTextColor(ContextCompat.getColor(this, R.color.diaCaducado));
                break;
            case Calendar.TUESDAY:

                lCo.setEnabled(false);

                mCo.setEnabled(false);

                lCe.setEnabled(false);

                mCe.setEnabled(false);

                xCo.setEnabled(false);

                xCe.setEnabled(false);
                lunes.setTextColor(ContextCompat.getColor(this, R.color.diaCaducado));
                martes.setTextColor(ContextCompat.getColor(this, R.color.diaCaducado));
                miercoles.setTextColor(ContextCompat.getColor(this, R.color.diaCaducado));
                break;
            case Calendar.WEDNESDAY:
                Log.d("","Miercoles");

                lCo.setEnabled(false);

                mCo.setEnabled(false);

                lCe.setEnabled(false);

                mCe.setEnabled(false);

                xCo.setEnabled(false);

                xCe.setEnabled(false);

                jCo.setEnabled(false);

                jCe.setEnabled(false);

                lunes.setTextColor(ContextCompat.getColor(this, R.color.diaCaducado));
                martes.setTextColor(ContextCompat.getColor(this, R.color.diaCaducado));
                miercoles.setTextColor(ContextCompat.getColor(this, R.color.diaCaducado));
                jueves.setTextColor(ContextCompat.getColor(this, R.color.diaCaducado));
                break;
            case Calendar.THURSDAY:

                lCo.setEnabled(false);

                mCo.setEnabled(false);

                lCe.setEnabled(false);

                mCe.setEnabled(false);

                xCo.setEnabled(false);

                xCe.setEnabled(false);

                jCo.setEnabled(false);

                jCe.setEnabled(false);

                vCo.setEnabled(false);

                vCe.setEnabled(false);

                lunes.setTextColor(ContextCompat.getColor(this, R.color.diaCaducado));
                martes.setTextColor(ContextCompat.getColor(this, R.color.diaCaducado));
                miercoles.setTextColor(ContextCompat.getColor(this, R.color.diaCaducado));
                jueves.setTextColor(ContextCompat.getColor(this, R.color.diaCaducado));
                viernes.setTextColor(ContextCompat.getColor(this, R.color.diaCaducado));
                break;
            case Calendar.FRIDAY:
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("Aviso");
                alert.setMessage("Ya han sido renovados los horarios. A partir de ahora la modificación afectará a los horarios de la semana entrante.");
                AlertDialog alerta = alert.create();
                alerta.show();
                break;
            case Calendar.SUNDAY:
                lCo.setEnabled(false);
                lunes.setTextColor(ContextCompat.getColor(this, R.color.diaCaducado));
                break;
            default:
                Log.d("Myapp", "Estamos en sábado o domingo");
                break;
        }

        /*AHORA CARGAMOS LA INFORMACIÓN GUARDADA DE LA ÚLTIMA VEZ QUE ENVIAMOS DATOS*/
        // cargarDatos();




        lCo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        globalParameters.setLuCo("L,X");
                        break;
                    case 1:
                        globalParameters.setLuCo("L,N");
                        break;
                    case 2:
                        globalParameters.setLuCo("L,T");
                        break;
                    case 3:
                        globalParameters.setLuCo("L,P");
                        break;
                    case 4:
                        globalParameters.setLuCo("L,B");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                globalParameters.setLuCo("L,X");
            }
        });
        lCe.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        globalParameters.setLuCe("L,X");
                        break;
                    case 1:
                        globalParameters.setLuCe("L,N");
                        break;
                    case 2:
                        globalParameters.setLuCe("L,T");
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                globalParameters.setLuCe("L,X");
            }
        });
        mCo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        globalParameters.setMaCo("M,X");
                        break;
                    case 1:
                        globalParameters.setMaCo("M,N");
                        break;
                    case 2:
                        globalParameters.setMaCo("M,T");
                        break;
                    case 3:
                        globalParameters.setMaCo("M,P");
                        break;
                    case 4:
                        globalParameters.setMaCo("M,B");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                globalParameters.setMaCo("M,X");
            }
        });
        mCe.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        globalParameters.setMaCe("M,X");
                        break;
                    case 1:
                        globalParameters.setMaCe("M,N");
                        break;
                    case 2:
                        globalParameters.setMaCe("M,T");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                globalParameters.setMaCe("M,X");
            }
        });
        xCo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        globalParameters.setMiCo("X,X");
                        break;
                    case 1:
                        globalParameters.setMiCo("X,N");
                        break;
                    case 2:
                        globalParameters.setMiCo("X,T");
                        break;
                    case 3:
                        globalParameters.setMiCo("X,P");
                        break;
                    case 4:
                        globalParameters.setMiCo("X,B");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                globalParameters.setMiCo("X,X");
            }
        });
        xCe.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        globalParameters.setMiCe("X,X");
                        break;
                    case 1:
                        globalParameters.setMiCe("X,N");
                        break;
                    case 2:
                        globalParameters.setMiCe("X,T");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                globalParameters.setMiCe("X,X");
            }
        });
        jCo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        globalParameters.setJuCo("J,X");
                        break;
                    case 1:
                        globalParameters.setJuCo("J,N");
                        break;
                    case 2:
                        globalParameters.setJuCo("J,T");
                        break;
                    case 3:
                        globalParameters.setJuCo("J,P");
                        break;
                    case 4:
                        globalParameters.setJuCo("J,B");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                globalParameters.setJuCo("J,X");
            }
        });
        jCe.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        globalParameters.setJuCe("J,X");
                        break;
                    case 1:
                        globalParameters.setJuCe("J,N");
                        break;
                    case 2:
                        globalParameters.setJuCe("J,T");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                globalParameters.setJuCe("J,X");
            }
        });
        vCo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        globalParameters.setViCo("V,X");
                        break;
                    case 1:
                        globalParameters.setViCo("V,N");
                        break;
                    case 2:
                        globalParameters.setViCo("V,T");
                        break;
                    case 3:
                        globalParameters.setViCo("V,P");
                        break;
                    case 4:
                        globalParameters.setViCo("V,B");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                globalParameters.setViCo("V,X");
            }
        });
        vCe.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        globalParameters.setViCe("V,X");
                        break;
                    case 1:
                        globalParameters.setViCe("V,N");
                        break;
                    case 2:
                        globalParameters.setViCe("V,T");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                globalParameters.setViCe("V,X");
            }
        });


    }

    public int queNumero(String string){

        switch (string.charAt(2)){
            case 'X':
                return 0;
            case 'N':
                return 1;
            case 'T':
                return 2;
            case 'P':
                return 3;
            case 'B':
                return 4;
        }
        return 0;
    }



}
