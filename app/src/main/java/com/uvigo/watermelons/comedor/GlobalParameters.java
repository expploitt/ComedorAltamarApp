package com.uvigo.watermelons.comedor;

import android.app.Activity;
import android.app.Application;

/**
 * Created by expploitt on 19/08/17.
 */

public class GlobalParameters extends Application {

    private String luCo;
    private String luCe;
    private String maCo;
    private String maCe;
    private String miCo;
    private String miCe;
    private String juCo;
    private String juCe;
    private String viCo;
    private String viCe;

    private String luCof;
    private String luCef;
    private String maCof;
    private String maCef;
    private String miCof;
    private String miCef;
    private String juCof;
    private String juCef;
    private String viCof;
    private String viCef;

    private String dni;

    public String getLuCo() {
        return luCo;
    }

    public void setLuCo(String luCo) {
        this.luCo = luCo;
    }

    public String getLuCe() {
        return luCe;
    }

    public void setLuCe(String luCe) {
        this.luCe = luCe;
    }

    public String getMaCo() {
        return maCo;
    }

    public void setMaCo(String maCo) {
        this.maCo = maCo;
    }

    public String getMaCe() {
        return maCe;
    }

    public void setMaCe(String maCe) {
        this.maCe = maCe;
    }

    public String getMiCo() {
        return miCo;
    }

    public void setMiCo(String miCo) {
        this.miCo = miCo;
    }

    public String getMiCe() {
        return miCe;
    }

    public void setMiCe(String miCe) {
        this.miCe = miCe;
    }

    public String getJuCo() {
        return juCo;
    }

    public void setJuCo(String juCo) {
        this.juCo = juCo;
    }

    public String getJuCe() {
        return juCe;
    }

    public void setJuCe(String juCe) {
        this.juCe = juCe;
    }

    public String getViCo() {
        return viCo;
    }

    public void setViCo(String viCo) {
        this.viCo = viCo;
    }

    public String getViCe() {
        return viCe;
    }

    public void setViCe(String viCe) {
        this.viCe = viCe;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getLuCof() {
        return luCof;
    }

    public void setLuCof(String luCof) {
        this.luCof = luCof;
    }

    public String getLuCef() {
        return luCef;
    }

    public void setLuCef(String luCef) {
        this.luCef = luCef;
    }

    public String getMaCof() {
        return maCof;
    }

    public void setMaCof(String maCof) {
        this.maCof = maCof;
    }

    public String getMaCef() {
        return maCef;
    }

    public void setMaCef(String maCef) {
        this.maCef = maCef;
    }

    public String getMiCof() {
        return miCof;
    }

    public void setMiCof(String miCof) {
        this.miCof = miCof;
    }

    public String getMiCef() {
        return miCef;
    }

    public void setMiCef(String miCef) {
        this.miCef = miCef;
    }

    public String getJuCof() {
        return juCof;
    }

    public void setJuCof(String juCof) {
        this.juCof = juCof;
    }

    public String getJuCef() {
        return juCef;
    }

    public void setJuCef(String juCef) {
        this.juCef = juCef;
    }

    public String getViCof() {
        return viCof;
    }

    public void setViCof(String viCof) {
        this.viCof = viCof;
    }

    public String getViCef() {
        return viCef;
    }

    public void setViCef(String viCef) {
        this.viCef = viCef;
    }
}
